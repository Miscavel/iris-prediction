# Iris Prediction C++ (DBSCAN & Naive Bayes Classifier)

Dataset consists of information regarding "Iris" flowers with the following attributes :
1. **Sepal length** (in cm)
2. **Sepal width** (in cm)
3. **Petal length** (in cm)
4. **Petal width** (in cm)

There are 3 different "Iris" types in which a flower can be classified into based on its attributes.

![](Screenshots/iris_type.png)

The iris-train.csv dataset is used to train the prediction models, which consists of :
1. 135 rows of data, with each row representing the attributes of a unique flower.
2. 1st to 4th column represents the flower's attributes, in the respective order as mentioned above.
3. 5th column represents the flower's "Iris" type.
4. Each column is separated by a comma (',').

The iris-test.csv dataset is used to test the accuracy of the prediction models, which consists of :
1. 15 rows of data, with each row representing the attributes of a unique flower.
2. 5 columns with specifications following iris-train.csv.

Hotkeys :
- Left Click (DBSCAN) : Choose a point on the Average KNN Distance Graph.
- Right Click (DBSCAN) : Calculates DBSCAN clusters based on the chosen point.
- Number key '1' : Shows Average KNN Distance Graph.
- Number key '2' : Shows DBSCAN clusters.
- Keyboard 'I' : DBSCAN Prediction.
- Keyboard 'N' : Naive Bayes Prediction.

Note :
1. Project is built in Visual Studio 2010.
2. For academic purposes, most of the algorithms are self-coded albeit not optimized.

# First Method : Density Based Clustering

**1. Choosing k-value in K-Nearest Neighbor (KNN)**

column_size = 4; (Sepal Length, Sepal Width, Petal Length, Petal Width)

k	= column_size;
	
	= 4;

**2. Plot Average KNN Distance Graph with k = 4 [Ascending]**

![](Screenshots/gdb-1.png)

**3. Choose a point at the elbow of the Graph**

![](Screenshots/gdb-2.png)

**4. Plot DBSCAN Clusters using the chosen parameters**

![](Screenshots/gdb-3.png)

column_size = 4; //(Sepal Length, Sepal Width, Petal Length, Petal Width)

req	= column_size + 1

	= 5;

rad	= 0.435. //(From Step 3)

From the DBSCAN, the following clusters are obtained :

- Cluster 0	: Majority Iris-setosa
- Cluster 1	: Majority Iris-versicolor
- Cluster 2	: Majority Iris-virginica

Cluster details can be found in results.txt

**5. Calculate prediction accuracy on train.csv and test.csv**

Prediction algorithm : 
- Identify the new point’s cluster using nearest neighbor search
- The new point’s class is equal to that cluster’s majority vote

train.csv accuracy

![](Screenshots/gdb-11.png)

test.csv accuracy

![](Screenshots/gdb-9.png)

**6. Comparison with points below / above elbow**

- Below elbow

![](Screenshots/gdb-4.png)

rad	= 0.28.

test.csv accuracy

![](Screenshots/gdb-8.png)

When the radius is too small, some points are excluded from the clusters thereby affecting the prediction accuracy.

- Above elbow

![](Screenshots/gdb-6.png)

rad	= 0.782.

test.csv accuracy

![](Screenshots/gdb-10.png)

When the radius is too large, clusters may lose their characteristics and merge with other clusters which also affect the prediction accuracy.

# Second Method : Naive Bayes Classification

**1. Calculate Mean and Variance for each [class, column] combination**

![](Screenshots/nb-2.png)

**2. Calculate prediction accuracy on train.csv and test.csv**

![](Screenshots/nb-3.png)

train.csv accuracy

![](Screenshots/nb-5.png)

test.csv accuracy

![](Screenshots/nb-4.png)